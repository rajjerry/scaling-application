var cp = require('child_process');
var child = cp.fork('./child');

child.on('message', function(m) {
    console.log('In parent, received from child: ' + m);
});

child.send('String will converted into uppercase');