// const { spawn } = require('child_process');
// const child = spawn('wc');

// process.stdin.pipe(child.stdin);

// child.stdout.on('data', (data) => {
//     console.log(`child stdout : \n ${data}`);
// })

const { spawn } = require('child_process');

const find = spawn('find', ['.', '-type', 'f']);
const wc = spawn('wc', ['-l']);

find.stdout.pipe(wc.stdin);

wc.stdout.on('data', (data) => {
    console.log(`Number of files ${data}`);
});