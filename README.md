execFile is used when we just need to execute an application and get the output. For example, we can use execFile to run an image-processing application like ImageMagick to convert an image from PNG to JPG format and we only care if it succeeds or not. execFile should not be used when the external application produces a large amount of data and we need to consume that data in real time manner.

As spawn returns a stream based object, it’s great for handling applications that produce large amounts of data or for working with data as it reads in. Ex. read the image using stream and spawn image of convert.

exec should be used when we need to utilize shell functionality such as pipe, redirects, backgrounding…